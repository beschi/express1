var express = require('express');
var router = express.Router();


router.get('/', function(req, res, next) {
  res.render('router-examples', {title: 'Router Examples', content: 'Simple routing'});
});

router.get('/ab.cd', function(req, res, next) {
  res.render('router-examples', {title: 'Router Examples', content: 'will match ab.cd'});
});

router.get('/ab?cd', function(req, res, next) {
  res.render('router-examples', {title: 'Router Examples', content: 'will match acd or abcd'});
});

router.get('/ab+cd', function(req, res, next) {
  res.render('router-examples', {title: 'Router Examples', content: 'will match abcd, abbcd, abbbcd...'});
});

router.get('/param1/:param1/param2/:param2', function(req, res, next) {
  console.log(req.params);
  res.render('router-examples', {title: 'Router Examples', content: 'with paramaters: /param1/:param1/param2/:param2; for example /param1/22/param2/new'});
});



module.exports = router;